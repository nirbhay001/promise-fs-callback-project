const fs = require('fs');
const path = require('path');


const directoryCreate = (nameDirectory) => {
    return new Promise((resolve, reject) => {

        const directoryPath = path.join(__dirname, nameDirectory);
        fs.mkdir(directoryPath, { recursive: true }, (error) => {
            if (error) {
                reject(error);
            } else {
                resolve(directoryPath);
            }
        });
    });
}

const createRandomJsonFile = (directoryPath, num) => {
    return new Promise((resolve, reject) => {
        for (let file = 1; file <= num; file++) {
            let fileName = Math.random().toString().slice(2) + '.json';
            let pathFile = path.join(directoryPath, fileName);
            fs.open(pathFile, 'w', (error) => {
                if (error) {
                    reject(error);
                }
            })
        }
        resolve(directoryPath);

        return directoryPath;
    })
};

const deleteFileSimultaneously = (directoryPath) => {
    return new Promise((resolve, reject) => {
        fs.readdir(directoryPath, (error, data) => {
            if (error) {
                reject(error);
            }
            else {
                data.map((fileName) => {
                    const filePath = path.join(directoryPath, fileName);
                    fs.unlink(filePath, (error) => {
                        if (error) {
                            reject(error);
                        }
                    })
                });
                resolve("All files deleted");
            }
        })
    })
};



module.exports.directoryCreate = directoryCreate;
module.exports.createRandomJsonFile = createRandomJsonFile;
module.exports.deleteFileSimultaneously = deleteFileSimultaneously;
