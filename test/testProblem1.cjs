const {directoryCreate,createRandomJsonFile,deleteFileSimultaneously} = require("../problem1.cjs");

directoryCreate('JsonDirectory')
.then((directory)=>{
   return createRandomJsonFile(directory,5);
})
.then((directory)=>{
    return deleteFileSimultaneously(directory)
})
.then((data)=>{
    console.log(data);
})
.catch((data)=>{
        console.log(data);
})