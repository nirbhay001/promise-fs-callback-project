let {convertContentUpperCase , readFile, convertContentLowerCase, sortContentOfNewFiles,readDeleteFiles}=require("../problem2.cjs")

readFile()
.then((data)=>{
    return convertContentUpperCase(data);
})
.then(()=>{
    return convertContentLowerCase();
})
.then(()=>{
    return sortContentOfNewFiles();
})
.then(()=>{
   return readDeleteFiles();
})
.then((data)=>{
    console.log(data);
})
.catch((data)=>{
    console.log(data);
})


